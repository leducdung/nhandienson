import { MongooseModule } from '@nestjs/mongoose';


export const databaseModule  = MongooseModule.forRootAsync({
  useFactory: () => ({
    uri: 'mongodb+srv://leducdung:leducdung@nhandien.5uwjo.mongodb.net/nhandien',
  }),
});