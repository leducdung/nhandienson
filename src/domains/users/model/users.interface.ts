
import { Document, Types } from 'mongoose'

export interface Users extends Document {
  userName?: string
  passWord?: string
  fullName?: string
  phone?: number
  email?:string
  facebookID?:string
  googleID?: string

}

export const fieldNeedToUseRegex = ['fullName']