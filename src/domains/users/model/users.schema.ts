import * as mongoose from 'mongoose'
import { UserStatus, UserRole } from '../../../constains/common'

const {
  Types: { ObjectId },
} = mongoose.Schema

export const UsersSchema = new mongoose.Schema({
  userName: { type: String, index: true, unique: true, sparse: true, require: true },
  passWord: { type: String, require: true },
  fullName: { type: String, default: null },
  phone: { type: Number, default: null },
  email:{ type: String, default: null },
  facebookID: { type: String, default: null },
  googleID: { type: String, default: null },

}, {
  timestamps: true,
})
