import { Module ,forwardRef } from '@nestjs/common';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersSchema } from './model/users.schema';
import { AuthModule } from '../../auth/auth.module';
import { databaseModule } from '../../database/database.module'
import { UsersController } from './users.controller';


export const UsersModel = MongooseModule.forFeature([
  {
    name: 'user',
    schema: UsersSchema,
  },
])

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => UsersModule),
    forwardRef(() => databaseModule),
    forwardRef(() => UsersModel),

  ],
  controllers: [UsersController],
  providers: [UsersService ],
  exports: [UsersService],
})
export class UsersModule {}