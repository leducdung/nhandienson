import { Document, Types } from 'mongoose';

export interface FaceRecognition extends Document {
  name?: String
  indentify?: Boolean
  image?: String
  createBy?: Types.ObjectId
  createdAt?: Date;
  updatedAt?: Date;
}

export const fieldNeedToUseRegex = [ 'title' ];
