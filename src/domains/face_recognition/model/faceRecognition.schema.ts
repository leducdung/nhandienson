import * as mongoose from 'mongoose';

const {
  Types: { ObjectId },
} = mongoose.Schema;

export const faceRecognitionSchema = new mongoose.Schema(
  {
    name: { type: String, default: null },
    indentify: { type: Boolean, default: null },
    image: { type: String, default: null },
    createBy:{ type: ObjectId, ref: 'user', default: null  }
  },
  {
    timestamps: true,
  },
);
