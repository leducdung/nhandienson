import {  ApiPropertyOptional } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { sortDirection, SortDirection } from '../../../constains/common';

export enum SortBy {
  id = '_id',
  title = 'title',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}

export class CreateFaceRecognitionDto {
  @ApiPropertyOptional()
  readonly createBy?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly name?: string;

  @ApiPropertyOptional()
  readonly indentify?: boolean;

  @ApiPropertyOptional()
  readonly image?: string;

}

export class DataUpdateFaceRecognitionDto {
  @ApiPropertyOptional()
  readonly createBy?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly name?: string;

  @ApiPropertyOptional()
  readonly indentify?: boolean;

  @ApiPropertyOptional()
  readonly image?: string;
}

export class ParamFaceRecognitionDto {
  @ApiPropertyOptional()
  readonly faceRecognitionID?: Types.ObjectId;
}

export class FindFaceRecognitionDto {
  @ApiPropertyOptional({ enum: SortBy })
  readonly sortBy?: SortBy = SortBy.id;

  @ApiPropertyOptional({ enum: sortDirection })
  readonly sortDirection?: SortDirection = SortDirection.asc;

  @ApiPropertyOptional()
  readonly limit?: number;

  @ApiPropertyOptional()
  readonly offset?: number;

  @ApiPropertyOptional()
  readonly cursor?: string;

  @ApiPropertyOptional()
  readonly createBy?: Types.ObjectId;

  @ApiPropertyOptional()
  readonly name?: string;

  @ApiPropertyOptional()
  readonly indentify?: boolean;

  @ApiPropertyOptional()
  readonly image?: string;
}
