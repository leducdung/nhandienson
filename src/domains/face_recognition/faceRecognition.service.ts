import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
export type User = any;
import {
  FaceRecognition,
  fieldNeedToUseRegex,
} from './model/faceRecognition.interface';
import { InjectModel } from '@nestjs/mongoose';
import { getNextCursor } from '../../helpers/gets';
import { buildFindingQuery } from '../../helpers/build';

@Injectable()
export class FaceRecognitionService {
  constructor(
    @InjectModel('FaceRecognition')
    private readonly faceRecognitionModel: Model<FaceRecognition>,
  ) {}

  async findOne({ data }: any): Promise<FaceRecognition | string> {
    try {
      const faceRecognition = await this.faceRecognitionModel
        .findOne(data)
        .populate('createBy')
        .exec();

      if (!faceRecognition) {
        return 'faceRecognition not found';
      }

      return faceRecognition;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findProduct(query): Promise<FaceRecognition | string> {
    try {
      const faceRecognition = await this.faceRecognitionModel
        .findOne(query)
        .populate('createBy')
      .exec();

      if (!faceRecognition) {
        return 'FaceRecognition not found';
      }

      return faceRecognition;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async createOne({ data }: any): Promise<FaceRecognition | undefined> {
    try {
      const faceRecognition = await new this.faceRecognitionModel(data)

      await faceRecognition.save();

      return await faceRecognition;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne({ data, query }): Promise<FaceRecognition | string> {
    try {
      const faceRecognition = await this.faceRecognitionModel
        .findOne(query)
        .populate('createBy')
      .exec();

      if (!faceRecognition) {
        return 'faceRecognition Not Found';
      }

      Object.assign(faceRecognition, data);

      await faceRecognition.save();

      return faceRecognition;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteOne(query): Promise<boolean | string> {
    try {
      const faceRecognition = await this.faceRecognitionModel.findOne(query).exec();

      if (!faceRecognition) {
        return 'faceRecognition not found';
      }

      await this.faceRecognitionModel.deleteOne(query).exec();

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findMany({ query }) {
    try {
      const {
        limit = 10,
        sortBy = '_id',
        offset = 0,
        ...queryWithoutSortByAndLimit
      } = query;

      const promises = [];

      const {
        sortingCondition,
        findingQuery,
        findAllQuery,
        hasPage,
      } = buildFindingQuery({
        query: {
          ...queryWithoutSortByAndLimit,
          sortBy,
          limit,
        },
        fieldNeedToUseRegex,
      });

      const limits = parseInt(limit);
      const skip = parseInt(offset);
      if (hasPage) {
        promises.push(
          this.faceRecognitionModel
            .find(findingQuery)
            .populate('createBy')
            .sort(sortingCondition)
            .limit(limits)
            .skip(skip)
            .exec(),
          this.faceRecognitionModel.countDocuments(findAllQuery).exec(),
        );
      }

      if (!hasPage) {
        promises.push(
          this.faceRecognitionModel
            .find(findingQuery)
            .populate('createBy')
            .exec(),
          this.faceRecognitionModel.countDocuments(findAllQuery).exec(),
        );
      }

      const [FaceRecognition, totalCount] = await Promise.all(promises);

      if (!FaceRecognition || !FaceRecognition.length) {
        return {
          cursor: 'END',
          totalCount,
          list: [],
        };
      }

      const nextCursor = getNextCursor({
        data: FaceRecognition,
        sortBy,
      });

      return {
        cursor: nextCursor,
        totalCount,
        list: FaceRecognition,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
