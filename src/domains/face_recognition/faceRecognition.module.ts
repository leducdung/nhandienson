import { Module, forwardRef } from '@nestjs/common';
import { FaceRecognitionService } from './faceRecognition.service';
import { MongooseModule } from '@nestjs/mongoose';
import { faceRecognitionSchema } from './model/faceRecognition.schema';
import { AuthModule } from '../../auth/auth.module';
import { databaseModule } from '../../database/database.module';
import { FaceRecognitionController } from './faceRecognition.controller';

export const faceRecognitionModel = MongooseModule.forFeature([
  {
    name: 'FaceRecognition',
    schema: faceRecognitionSchema,
  },
]);

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => databaseModule),
    forwardRef(() => faceRecognitionModel),
  ],
  controllers: [FaceRecognitionController],
  providers: [FaceRecognitionService],
  exports: [FaceRecognitionService],
})
export class FaceRecognitionModule {}
