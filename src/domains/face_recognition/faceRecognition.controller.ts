import {
  Controller,
  Get,
  Request,
  Post,
  UseGuards,
  Body,
  Put,
  Param,
  Query,
  Delete,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/jwt/jwt-auth.guard';
import { AuthService } from '../../auth/auth.service';
import {
  CreateFaceRecognitionDto,
  DataUpdateFaceRecognitionDto,
  ParamFaceRecognitionDto,
  FindFaceRecognitionDto,
} from './model/faceRecognition.dto';
import { ApiTags } from '@nestjs/swagger';
import { FaceRecognitionService } from './faceRecognition.service';

@ApiTags('faceRecognition')
@Controller('')
export class FaceRecognitionController {
  constructor(private readonly faceRecognitionService: FaceRecognitionService) {}

  @UseGuards(JwtAuthGuard)
  @Get('faceRecognition')
  async findMany(
    @Query() query: FindFaceRecognitionDto,
    @Request() { req },
  ) {
    try {
      const faceRecognition = await this.faceRecognitionService.findMany({
        query,
      });

      return faceRecognition;
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('faceRecognition/:faceRecognitionID')
  async findOne(
    @Param() { faceRecognitionID }: ParamFaceRecognitionDto,
    @Request() req,
  ) {
    try {
      return this.faceRecognitionService.findProduct({ _id: faceRecognitionID });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('faceRecognition')
  async createOne(
    @Body() createFaceRecognitionDto: CreateFaceRecognitionDto,
    @Request() req,
  ) {
    try {
      return this.faceRecognitionService.createOne({
        data: {
          createBy: req.user.resultUser._id,
          ...createFaceRecognitionDto,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete('faceRecognition/:faceRecognitionID')
  async Delete(@Param() faceRecognitionID: ParamFaceRecognitionDto, @Request() req) {
    try {
      return this.faceRecognitionService.deleteOne({
        _id: faceRecognitionID.faceRecognitionID,
      });
    } catch (error) {
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Put('faceRecognition/:faceRecognitionID')
  async updateOne(
    @Param() faceRecognitionID: ParamFaceRecognitionDto,
    @Body() data: DataUpdateFaceRecognitionDto,
    @Request() req,
  ) {
    try {
      return this.faceRecognitionService.updateOne({
        data,
        query: { _id: faceRecognitionID.faceRecognitionID },
      });
    } catch (error) {
      throw error;
    }
  }
}
