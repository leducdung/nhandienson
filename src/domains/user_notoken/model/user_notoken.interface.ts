
import { Document, Types } from 'mongoose'

export interface User_no_token extends Document {
  userName?: string
  passWord?: string
  fullName?: string
  phone?: number
  email?:string
  facebookID?:string
  googleID?: string

}

export const fieldNeedToUseRegex = ['fullName']