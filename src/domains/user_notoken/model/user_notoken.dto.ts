import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Types } from 'mongoose'
import { sortDirection , SortDirection } from '../../../constains/common'

export enum SortBy {
  id = '_id',
  title = 'title',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}


export class CreateUser_no_tokenDto {
  @ApiPropertyOptional()
  readonly userName?: string

  @ApiPropertyOptional()
  readonly passWord?: string

  @ApiPropertyOptional()
  readonly fullName?: string

  @ApiPropertyOptional()
  readonly phone?: number

  @ApiPropertyOptional()
  readonly email?: string

  @ApiPropertyOptional()
  readonly facebookID?: string

  @ApiPropertyOptional()
  readonly googleID?: string
}

export class DataUpdateUser_no_tokenDto {
  @ApiPropertyOptional()
  readonly userName?: string

  @ApiPropertyOptional()
  readonly passWord?: string

  @ApiPropertyOptional()
  readonly fullName?: string

  @ApiPropertyOptional()
  readonly phone?: number

  @ApiPropertyOptional()
  readonly email?: string

  @ApiPropertyOptional()
  readonly facebookID?: string

  @ApiPropertyOptional()
  readonly googleID?: string
}

export class ParamUser_no_tokenDto {
  @ApiPropertyOptional()
  readonly userID?: Types.ObjectId
}

export class FindManyUser_no_tokenDto {
  @ApiPropertyOptional({ enum: SortBy })
  readonly sortBy?: SortBy = SortBy.id

  @ApiPropertyOptional({ enum: sortDirection })
  readonly sortDirection?: SortDirection = SortDirection.asc

  @ApiPropertyOptional()
  readonly limit?: number

  @ApiPropertyOptional()
  readonly offset?: number

  @ApiPropertyOptional()
  readonly cursor?: string

  @ApiPropertyOptional()
  readonly userName?: string

  @ApiPropertyOptional()
  readonly passWord?: string

  @ApiPropertyOptional()
  readonly fullName?: string

  @ApiPropertyOptional()
  readonly phone?: number

  @ApiPropertyOptional()
  readonly email?: string

  @ApiPropertyOptional()
  readonly facebookID?: string

  @ApiPropertyOptional()
  readonly googleID?: string
}
