import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose'
export type User = any;
import { User_no_token, fieldNeedToUseRegex } from './model/user_notoken.interface';
import { InjectModel } from '@nestjs/mongoose'
import { getNextCursor } from '../../helpers/gets'
import { buildFindingQuery } from '../../helpers/build'
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {

  constructor(
    @InjectModel('User_no_token')
    private readonly user_no_tokenModel: Model<User_no_token>,
  ) { }

  async findOne({ data }: any): Promise<User_no_token> {
    try {
      const a = await this.user_no_tokenModel.findOne(data).exec()
      return a
    } catch (error) {
      return Promise.reject(error)
    }
  }


  async findOneUser(query): Promise<User_no_token> {
    try {
      const user = await this.user_no_tokenModel.findOne(query)
        .exec()
      return user
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async login(data): Promise<any> {
    try {
      const user = await this.user_no_tokenModel.findOne({userName:data.userName}).exec()

      if (!user) {
        return { message: 'Username not found' };
      }

      const isMatch = await bcrypt.compare(data.passWord, user.passWord);

      if (!isMatch) {
        return { message: 'Incorrect password' };
      }

      return {
        message: 'Success',
        user
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }



  async createOne({ data }): Promise<User_no_token | undefined> {
    try {
      const user = await new this.user_no_tokenModel(data)

      await user.save()

      return await user
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async updateOne({ data, _id }): Promise<User_no_token | string> {
    try {

      const user = await this.user_no_tokenModel.findOne(_id)
        .exec()

      if (!user) {
        return 'User Not Found'
      }

      Object.assign(user, data)

      await user.save()

      return user
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async deleteOne({ query }): Promise<boolean | string> {
    try {
      const user = await this.user_no_tokenModel.findOne(query).exec()

      if (!user) {
        return 'User not found'
      }

      await this.user_no_tokenModel.deleteOne(query).exec()

      return true

    } catch (error) {
      return Promise.reject(error)
    }
  }


  async findMany({ query }) {
    try {
      const { limit = 10, sortBy = '_id', offset = 0, ...queryWithoutSortByAndLimit } = query

      const promises = []

      const {
        sortingCondition,
        findingQuery,
        findAllQuery,
        hasPage,
      } = buildFindingQuery({
        query: {
          ...queryWithoutSortByAndLimit,
          sortBy,
          limit,
        },
        fieldNeedToUseRegex,
      })

      const limits = parseInt(limit)
      const skip = parseInt(offset)
      if (hasPage) {
        promises.push(
          this.user_no_tokenModel.find(findingQuery)
            .sort(sortingCondition)
            .limit(limits)
            .skip(skip)
            .exec(),
          this.user_no_tokenModel.countDocuments(findAllQuery)
            .exec(),
        )
      }

      if (!hasPage) {
        promises.push(
          this.user_no_tokenModel.find(findingQuery)
            .exec(),
          this.user_no_tokenModel.countDocuments(findAllQuery)
            .exec(),
        )
      }

      const [ documents, totalCount ] = await Promise.all(promises)

      if (!documents || !documents.length) {
        return {
          cursor: 'END',
          totalCount,
          list: [],
        }
      }

      const nextCursor = getNextCursor({
        data: documents,
        sortBy,
      })

      return {
        cursor: nextCursor,
        totalCount,
        list: documents,
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }

}