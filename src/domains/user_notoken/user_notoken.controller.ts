import { Controller, Get, Request, Post, UseGuards,Body , Put , Param, Query} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/jwt/jwt-auth.guard';
import { LocalAuthGuard } from '../../auth/local/local-auth.guard';
import { AuthService } from '../../auth/auth.service';
import { CreateUser_no_tokenDto , FindManyUser_no_tokenDto,
ParamUser_no_tokenDto,  } from './model/user_notoken.dto';
import { ApiTags } from '@nestjs/swagger'
import { UsersService } from './user_notoken.service';
import * as bcrypt from 'bcrypt'

@ApiTags('User_no_token')
@Controller('User_no_token')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private authService: AuthService,
) { }

  @Get('profile/findMany')
  async findMany(
    @Query() query : FindManyUser_no_tokenDto,
  ){
    try {
      const users = await this.usersService.findMany({
        query,
      })

      return users
    } catch (error) {
      throw error
    }
  }

  @UseGuards()
  @Post('register')
  async createOne(
    @Body() CreateUserDto: CreateUser_no_tokenDto,
  ){
    try {

      const saltOrRounds = await 10;

      const hash = await bcrypt.hash(CreateUserDto.passWord ,saltOrRounds);

      return await this.usersService.createOne({
        data : {
          ...CreateUserDto,
          passWord:hash,

        }
      })
    } catch (error) {
      return error
    }
  }

  @UseGuards( )
  @Post('/login')
  async login(
    @Body() createUserDto:CreateUser_no_tokenDto) {
    try {
      return this.usersService.login(createUserDto);
    } catch (error) {
      throw error
    }
  }

  @Get('profile/:_id')
  async findOne(
    @Param() _id : ParamUser_no_tokenDto,
  ) {
    try {
      console.log(_id);
      return this.usersService.findOneUser(_id)
    } catch (error) {
      throw error
    }
  }

  @Put('profile/:_id')
  async updateOne(
    @Param() _id  : ParamUser_no_tokenDto,
    @Body() data : CreateUser_no_tokenDto,
  ) {
    try {
      return this.usersService.updateOne({data,_id})
    } catch (error) {
      throw error
    }
  }

}