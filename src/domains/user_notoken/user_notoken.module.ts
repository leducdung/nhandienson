import { Module ,forwardRef } from '@nestjs/common';
import { UsersService } from './user_notoken.service';
import { MongooseModule } from '@nestjs/mongoose';
import { User_no_tokenSchema } from './model/user_notoken.schema';
import { AuthModule } from '../../auth/auth.module';
import { databaseModule } from '../../database/database.module'
import { UsersController } from './user_notoken.controller';


export const User_no_tokenModel = MongooseModule.forFeature([
  {
    name: 'User_no_token',
    schema: User_no_tokenSchema,
  },
])

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => User_no_tokenModule),
    forwardRef(() => databaseModule),
    forwardRef(() => User_no_tokenModel),

  ],
  controllers: [UsersController],
  providers: [UsersService ],
  exports: [UsersService],
})

export class User_no_tokenModule {}