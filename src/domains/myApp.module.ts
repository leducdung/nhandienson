import { forwardRef, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { FaceRecognitionModule } from './face_recognition/faceRecognition.module';
import { UsersModule } from './users/users.module';
import { User_no_tokenModule } from './user_notoken/user_notoken.module';


export const myAppImPortModules = [
  ConfigModule.forRoot(),
  forwardRef(() => UsersModule),
  forwardRef(() => User_no_tokenModule),
  forwardRef(() => FaceRecognitionModule),

]

@Module({
  imports: myAppImPortModules,
})
export class MyAppModule { }