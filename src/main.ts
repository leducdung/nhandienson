import { NestFactory } from '@nestjs/core';
import { MyAppModule } from './domains/myApp.module'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import { UsersModule } from './domains/users/users.module';
import { User_no_tokenModule } from './domains/user_notoken/user_notoken.module';
import { FaceRecognitionModule } from './domains/face_recognition/faceRecognition.module';

async function bootstrap() {
    const app = await NestFactory.create(MyAppModule);

    const options =new DocumentBuilder()
    .setTitle('Cats example')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .addTag('HomeMarket')
    .build();

    const apiDocument = SwaggerModule.createDocument(app, options,{
        include:[
          UsersModule,
          User_no_tokenModule,
          FaceRecognitionModule
        ],
    });

    SwaggerModule.setup('api', app, apiDocument);

    app.enableCors();

    await app.listen(process.env.PORT || 3000 , function () {
      console.log('Server started.......');
    });
}
bootstrap();